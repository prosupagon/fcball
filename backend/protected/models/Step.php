<?php

/**
 * This is the model class for table "step".
 *
 * The followings are the available columns in table 'step':
 * @property integer $id
 * @property string $date
 * @property integer $advisor_id
 * @property integer $score
 * @property integer $team_1
 * @property string $team_1_score
 * @property integer $team_1_status
 * @property integer $team_2
 * @property string $team_2_score
 * @property integer $team_2_status
 * @property integer $team_3
 * @property string $team_3_score
 * @property integer $team_3_status
 * @property integer $team_4
 * @property string $team_4_score
 * @property integer $team_4_status
 * @property string $createdate
 * @property string $lastupdate
 */
class Step extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'step';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, advisor_id, score, team_1, team_1_score, team_1_status, team_2, team_2_score, team_2_status, team_3, team_3_score, team_3_status, team_4, team_4_score, team_4_status, createdate, lastupdate', 'required'),
			array('advisor_id, score, team_1, team_1_status, team_2, team_2_status, team_3, team_3_status, team_4, team_4_status', 'numerical', 'integerOnly'=>true),
			array('team_1_score, team_2_score, team_3_score, team_4_score', 'length', 'max'=>300),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, advisor_id, score, team_1, team_1_score, team_1_status, team_2, team_2_score, team_2_status, team_3, team_3_score, team_3_status, team_4, team_4_score, team_4_status, createdate, lastupdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'advisor_id' => 'Advisor',
			'score' => 'Score',
			'team_1' => 'Team 1',
			'team_1_score' => 'Team 1 Score',
			'team_1_status' => 'Team 1 Status',
			'team_2' => 'Team 2',
			'team_2_score' => 'Team 2 Score',
			'team_2_status' => 'Team 2 Status',
			'team_3' => 'Team 3',
			'team_3_score' => 'Team 3 Score',
			'team_3_status' => 'Team 3 Status',
			'team_4' => 'Team 4',
			'team_4_score' => 'Team 4 Score',
			'team_4_status' => 'Team 4 Status',
			'createdate' => 'Createdate',
			'lastupdate' => 'Lastupdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('advisor_id',$this->advisor_id);
		$criteria->compare('score',$this->score);
		$criteria->compare('team_1',$this->team_1);
		$criteria->compare('team_1_score',$this->team_1_score,true);
		$criteria->compare('team_1_status',$this->team_1_status);
		$criteria->compare('team_2',$this->team_2);
		$criteria->compare('team_2_score',$this->team_2_score,true);
		$criteria->compare('team_2_status',$this->team_2_status);
		$criteria->compare('team_3',$this->team_3);
		$criteria->compare('team_3_score',$this->team_3_score,true);
		$criteria->compare('team_3_status',$this->team_3_status);
		$criteria->compare('team_4',$this->team_4);
		$criteria->compare('team_4_score',$this->team_4_score,true);
		$criteria->compare('team_4_status',$this->team_4_status);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Step the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
