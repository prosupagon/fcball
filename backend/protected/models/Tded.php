<?php

/**
 * This is the model class for table "tded".
 *
 * The followings are the available columns in table 'tded':
 * @property integer $id
 * @property string $date
 * @property integer $category_id
 * @property string $league_id
 * @property string $time
 * @property integer $team_a
 * @property integer $team_b
 * @property integer $hilight
 * @property string $price_ball
 * @property string $price_score
 * @property string $home
 * @property string $away
 * @property string $team_a_score
 * @property string $view
 * @property string $high
 * @property string $low
 * @property string $confirm
 * @property string $createdate
 * @property string $lastupdate
 * @property integer $team_b_score
 */
class Tded extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tded';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, category_id, league_id, time, team_a, team_b, hilight, price_ball, price_score, home, away, team_a_score, view, high, low, confirm, createdate, lastupdate, team_b_score', 'required'),
			array('category_id, team_a, team_b, hilight, team_b_score', 'numerical', 'integerOnly'=>true),
			array('league_id, price_ball, price_score, team_a_score, view, confirm', 'length', 'max'=>300),
			array('home, away, high, low', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, category_id, league_id, time, team_a, team_b, hilight, price_ball, price_score, home, away, team_a_score, view, high, low, confirm, createdate, lastupdate, team_b_score', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'category_id' => 'Category',
			'league_id' => 'League',
			'time' => 'Time',
			'team_a' => 'Team A',
			'team_b' => 'Team B',
			'hilight' => 'Hilight',
			'price_ball' => 'Price Ball',
			'price_score' => 'Price Score',
			'home' => 'Home',
			'away' => 'Away',
			'team_a_score' => 'Team A Score',
			'view' => 'View',
			'high' => 'High',
			'low' => 'Low',
			'confirm' => 'Confirm',
			'createdate' => 'Createdate',
			'lastupdate' => 'Lastupdate',
			'team_b_score' => 'Team B Score',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('league_id',$this->league_id,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('team_a',$this->team_a);
		$criteria->compare('team_b',$this->team_b);
		$criteria->compare('hilight',$this->hilight);
		$criteria->compare('price_ball',$this->price_ball,true);
		$criteria->compare('price_score',$this->price_score,true);
		$criteria->compare('home',$this->home,true);
		$criteria->compare('away',$this->away,true);
		$criteria->compare('team_a_score',$this->team_a_score,true);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('high',$this->high,true);
		$criteria->compare('low',$this->low,true);
		$criteria->compare('confirm',$this->confirm,true);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('lastupdate',$this->lastupdate,true);
		$criteria->compare('team_b_score',$this->team_b_score);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tded the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
