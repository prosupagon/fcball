<?php

// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=localhost;dbname=cp087732_fcballstep',
	'emulatePrepare' => true,
	'username' => 'cp087732',
	'password' => 'P@ssw0rd!@#$',
	'charset' => 'utf8',
);