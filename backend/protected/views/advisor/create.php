<?php
/* @var $this AdvisorController */
/* @var $model Advisor */

$this->breadcrumbs=array(
	'Advisors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Advisor', 'url'=>array('index')),
	array('label'=>'Manage Advisor', 'url'=>array('admin')),
);
?>

<h1>Create Advisor</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>