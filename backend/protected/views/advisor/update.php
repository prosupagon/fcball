<?php
/* @var $this AdvisorController */
/* @var $model Advisor */

$this->breadcrumbs=array(
	'Advisors'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Advisor', 'url'=>array('index')),
	array('label'=>'Create Advisor', 'url'=>array('create')),
	array('label'=>'View Advisor', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Advisor', 'url'=>array('admin')),
);
?>

<h1>Update Advisor <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>