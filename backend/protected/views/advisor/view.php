<?php
/* @var $this AdvisorController */
/* @var $model Advisor */

$this->breadcrumbs=array(
	'Advisors'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Advisor', 'url'=>array('index')),
	array('label'=>'Create Advisor', 'url'=>array('create')),
	array('label'=>'Update Advisor', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Advisor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Advisor', 'url'=>array('admin')),
);
?>

<h1>View Advisor #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'lineid',
		'createdate',
		'lastupdate',
	),
)); ?>
