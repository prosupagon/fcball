<?php
/* @var $this AdvisorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Advisors',
);

$this->menu=array(
	array('label'=>'Create Advisor', 'url'=>array('create')),
	array('label'=>'Manage Advisor', 'url'=>array('admin')),
);
?>

<h1>Advisors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
