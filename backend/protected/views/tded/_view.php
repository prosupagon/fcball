<?php
/* @var $this TdedController */
/* @var $data Tded */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::encode($data->category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('league_id')); ?>:</b>
	<?php echo CHtml::encode($data->league_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time')); ?>:</b>
	<?php echo CHtml::encode($data->time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_a')); ?>:</b>
	<?php echo CHtml::encode($data->team_a); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_b')); ?>:</b>
	<?php echo CHtml::encode($data->team_b); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('hilight')); ?>:</b>
	<?php echo CHtml::encode($data->hilight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_ball')); ?>:</b>
	<?php echo CHtml::encode($data->price_ball); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price_score')); ?>:</b>
	<?php echo CHtml::encode($data->price_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('home')); ?>:</b>
	<?php echo CHtml::encode($data->home); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('away')); ?>:</b>
	<?php echo CHtml::encode($data->away); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_a_score')); ?>:</b>
	<?php echo CHtml::encode($data->team_a_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('view')); ?>:</b>
	<?php echo CHtml::encode($data->view); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('high')); ?>:</b>
	<?php echo CHtml::encode($data->high); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('low')); ?>:</b>
	<?php echo CHtml::encode($data->low); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('confirm')); ?>:</b>
	<?php echo CHtml::encode($data->confirm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastupdate')); ?>:</b>
	<?php echo CHtml::encode($data->lastupdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_b_score')); ?>:</b>
	<?php echo CHtml::encode($data->team_b_score); ?>
	<br />

	*/ ?>

</div>