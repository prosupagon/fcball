<?php
/* @var $this TdedController */
/* @var $model Tded */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category_id'); ?>
		<?php echo $form->textField($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'league_id'); ?>
		<?php echo $form->textField($model,'league_id',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'time'); ?>
		<?php echo $form->textField($model,'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_a'); ?>
		<?php echo $form->textField($model,'team_a'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_b'); ?>
		<?php echo $form->textField($model,'team_b'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hilight'); ?>
		<?php echo $form->textField($model,'hilight'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_ball'); ?>
		<?php echo $form->textField($model,'price_ball',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_score'); ?>
		<?php echo $form->textField($model,'price_score',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'home'); ?>
		<?php echo $form->textField($model,'home',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'away'); ?>
		<?php echo $form->textField($model,'away',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_a_score'); ?>
		<?php echo $form->textField($model,'team_a_score',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'view'); ?>
		<?php echo $form->textField($model,'view',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'high'); ?>
		<?php echo $form->textField($model,'high',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'low'); ?>
		<?php echo $form->textField($model,'low',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'confirm'); ?>
		<?php echo $form->textField($model,'confirm',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastupdate'); ?>
		<?php echo $form->textField($model,'lastupdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_b_score'); ?>
		<?php echo $form->textField($model,'team_b_score'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->