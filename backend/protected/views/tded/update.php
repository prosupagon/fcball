<?php
/* @var $this TdedController */
/* @var $model Tded */

$this->breadcrumbs=array(
	'Tdeds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tded', 'url'=>array('index')),
	array('label'=>'Create Tded', 'url'=>array('create')),
	array('label'=>'View Tded', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Tded', 'url'=>array('admin')),
);
?>

<h1>Update Tded <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>