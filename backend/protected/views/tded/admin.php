<?php
/* @var $this TdedController */
/* @var $model Tded */

$this->breadcrumbs=array(
	'Tdeds'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Tded', 'url'=>array('index')),
	array('label'=>'Create Tded', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tded-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tdeds</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tded-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'date',
		'category_id',
		'league_id',
		'time',
		'team_a',
		/*
		'team_b',
		'hilight',
		'price_ball',
		'price_score',
		'home',
		'away',
		'team_a_score',
		'view',
		'high',
		'low',
		'confirm',
		'createdate',
		'lastupdate',
		'team_b_score',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
