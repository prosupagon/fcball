<?php
/* @var $this TdedController */
/* @var $model Tded */

$this->breadcrumbs=array(
	'Tdeds'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tded', 'url'=>array('index')),
	array('label'=>'Manage Tded', 'url'=>array('admin')),
);
?>

<h1>Create Tded</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>