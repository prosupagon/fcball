<?php
/* @var $this TdedController */
/* @var $model Tded */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tded-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->textField($model,'category_id'); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'league_id'); ?>
		<?php echo $form->textField($model,'league_id',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'league_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'time'); ?>
		<?php echo $form->textField($model,'time'); ?>
		<?php echo $form->error($model,'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_a'); ?>
		<?php echo $form->textField($model,'team_a'); ?>
		<?php echo $form->error($model,'team_a'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_b'); ?>
		<?php echo $form->textField($model,'team_b'); ?>
		<?php echo $form->error($model,'team_b'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hilight'); ?>
		<?php echo $form->textField($model,'hilight'); ?>
		<?php echo $form->error($model,'hilight'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price_ball'); ?>
		<?php echo $form->textField($model,'price_ball',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'price_ball'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price_score'); ?>
		<?php echo $form->textField($model,'price_score',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'price_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'home'); ?>
		<?php echo $form->textField($model,'home',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'home'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'away'); ?>
		<?php echo $form->textField($model,'away',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'away'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_a_score'); ?>
		<?php echo $form->textField($model,'team_a_score',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'team_a_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'view'); ?>
		<?php echo $form->textField($model,'view',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'view'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'high'); ?>
		<?php echo $form->textField($model,'high',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'high'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'low'); ?>
		<?php echo $form->textField($model,'low',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'low'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'confirm'); ?>
		<?php echo $form->textField($model,'confirm',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'confirm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastupdate'); ?>
		<?php echo $form->textField($model,'lastupdate'); ?>
		<?php echo $form->error($model,'lastupdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_b_score'); ?>
		<?php echo $form->textField($model,'team_b_score'); ?>
		<?php echo $form->error($model,'team_b_score'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->