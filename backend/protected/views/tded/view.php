<?php
/* @var $this TdedController */
/* @var $model Tded */

$this->breadcrumbs=array(
	'Tdeds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Tded', 'url'=>array('index')),
	array('label'=>'Create Tded', 'url'=>array('create')),
	array('label'=>'Update Tded', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Tded', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tded', 'url'=>array('admin')),
);
?>

<h1>View Tded #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'category_id',
		'league_id',
		'time',
		'team_a',
		'team_b',
		'hilight',
		'price_ball',
		'price_score',
		'home',
		'away',
		'team_a_score',
		'view',
		'high',
		'low',
		'confirm',
		'createdate',
		'lastupdate',
		'team_b_score',
	),
)); ?>
