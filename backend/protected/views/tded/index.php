<?php
/* @var $this TdedController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tdeds',
);

$this->menu=array(
	array('label'=>'Create Tded', 'url'=>array('create')),
	array('label'=>'Manage Tded', 'url'=>array('admin')),
);
?>

<h1>Tdeds</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
