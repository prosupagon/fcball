<?php
/* @var $this StepController */
/* @var $model Step */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'step-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'advisor_id'); ?>
		<?php echo $form->textField($model,'advisor_id'); ?>
		<?php echo $form->error($model,'advisor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'score'); ?>
		<?php echo $form->textField($model,'score'); ?>
		<?php echo $form->error($model,'score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_1'); ?>
		<?php echo $form->textField($model,'team_1'); ?>
		<?php echo $form->error($model,'team_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_1_score'); ?>
		<?php echo $form->textField($model,'team_1_score',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'team_1_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_1_status'); ?>
		<?php echo $form->textField($model,'team_1_status'); ?>
		<?php echo $form->error($model,'team_1_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_2'); ?>
		<?php echo $form->textField($model,'team_2'); ?>
		<?php echo $form->error($model,'team_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_2_score'); ?>
		<?php echo $form->textField($model,'team_2_score',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'team_2_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_2_status'); ?>
		<?php echo $form->textField($model,'team_2_status'); ?>
		<?php echo $form->error($model,'team_2_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_3'); ?>
		<?php echo $form->textField($model,'team_3'); ?>
		<?php echo $form->error($model,'team_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_3_score'); ?>
		<?php echo $form->textField($model,'team_3_score',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'team_3_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_3_status'); ?>
		<?php echo $form->textField($model,'team_3_status'); ?>
		<?php echo $form->error($model,'team_3_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_4'); ?>
		<?php echo $form->textField($model,'team_4'); ?>
		<?php echo $form->error($model,'team_4'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_4_score'); ?>
		<?php echo $form->textField($model,'team_4_score',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'team_4_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'team_4_status'); ?>
		<?php echo $form->textField($model,'team_4_status'); ?>
		<?php echo $form->error($model,'team_4_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastupdate'); ?>
		<?php echo $form->textField($model,'lastupdate'); ?>
		<?php echo $form->error($model,'lastupdate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->