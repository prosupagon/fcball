<?php
/* @var $this StepController */
/* @var $model Step */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'advisor_id'); ?>
		<?php echo $form->textField($model,'advisor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'score'); ?>
		<?php echo $form->textField($model,'score'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_1'); ?>
		<?php echo $form->textField($model,'team_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_1_score'); ?>
		<?php echo $form->textField($model,'team_1_score',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_1_status'); ?>
		<?php echo $form->textField($model,'team_1_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_2'); ?>
		<?php echo $form->textField($model,'team_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_2_score'); ?>
		<?php echo $form->textField($model,'team_2_score',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_2_status'); ?>
		<?php echo $form->textField($model,'team_2_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_3'); ?>
		<?php echo $form->textField($model,'team_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_3_score'); ?>
		<?php echo $form->textField($model,'team_3_score',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_3_status'); ?>
		<?php echo $form->textField($model,'team_3_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_4'); ?>
		<?php echo $form->textField($model,'team_4'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_4_score'); ?>
		<?php echo $form->textField($model,'team_4_score',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team_4_status'); ?>
		<?php echo $form->textField($model,'team_4_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastupdate'); ?>
		<?php echo $form->textField($model,'lastupdate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->