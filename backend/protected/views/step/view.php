<?php
/* @var $this StepController */
/* @var $model Step */

$this->breadcrumbs=array(
	'Steps'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Step', 'url'=>array('index')),
	array('label'=>'Create Step', 'url'=>array('create')),
	array('label'=>'Update Step', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Step', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Step', 'url'=>array('admin')),
);
?>

<h1>View Step #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'advisor_id',
		'score',
		'team_1',
		'team_1_score',
		'team_1_status',
		'team_2',
		'team_2_score',
		'team_2_status',
		'team_3',
		'team_3_score',
		'team_3_status',
		'team_4',
		'team_4_score',
		'team_4_status',
		'createdate',
		'lastupdate',
	),
)); ?>
