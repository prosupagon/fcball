<?php
/* @var $this StepController */
/* @var $data Step */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advisor_id')); ?>:</b>
	<?php echo CHtml::encode($data->advisor_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('score')); ?>:</b>
	<?php echo CHtml::encode($data->score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_1')); ?>:</b>
	<?php echo CHtml::encode($data->team_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_1_score')); ?>:</b>
	<?php echo CHtml::encode($data->team_1_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_1_status')); ?>:</b>
	<?php echo CHtml::encode($data->team_1_status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('team_2')); ?>:</b>
	<?php echo CHtml::encode($data->team_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_2_score')); ?>:</b>
	<?php echo CHtml::encode($data->team_2_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_2_status')); ?>:</b>
	<?php echo CHtml::encode($data->team_2_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_3')); ?>:</b>
	<?php echo CHtml::encode($data->team_3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_3_score')); ?>:</b>
	<?php echo CHtml::encode($data->team_3_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_3_status')); ?>:</b>
	<?php echo CHtml::encode($data->team_3_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_4')); ?>:</b>
	<?php echo CHtml::encode($data->team_4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_4_score')); ?>:</b>
	<?php echo CHtml::encode($data->team_4_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team_4_status')); ?>:</b>
	<?php echo CHtml::encode($data->team_4_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastupdate')); ?>:</b>
	<?php echo CHtml::encode($data->lastupdate); ?>
	<br />

	*/ ?>

</div>