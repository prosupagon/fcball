<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/js/jquery-3.1.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/jquery.datetimepicker.min.css">
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/jquery.datetimepicker.full.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/jquery.dataTables.js"></script>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/select2.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/default.css">
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/address.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/dist/select2.full.min.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?> :: Default</title>
</head>

<body>

<div id="main-page">
	<div id="main-header">
		<div id="main-header-left">
			<?php echo CHtml::encode(Yii::app()->name); ?>
		</div>
		<div id="main-header-right">
			<?php if(Yii::app()->user->isGuest): ?>
				<a href="<?=Yii::app()->createUrl('site/login')?>" title="">Login</a>
			<?php else: ?> 
				<a href="<?=Yii::app()->createUrl('site/logout')?>" title="">Logout (<?=Yii::app()->user->name?>)</a>
			<?php endif; ?>
			<div class="menuprofile">
				<div class="bar1"></div>
				<div class="bar2"></div>
				<div class="bar3"></div>
			</div> 
		</div>
	</div>
	<div id="main-body">
		<div id="main-sidebar">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'Home', 'url'=>array('/site/index')),
					array('label'=>'Advisor', 'url'=>array('/advisor')),
					array('label'=>'TDED', 'url'=>array('/tded')),
					array('label'=>'Step', 'url'=>array('/step')),
				),
			)); ?>
		</div>
		<div id="main-content">
			<?php echo $content; ?>
		</div>
		<div id="main-profile">
			QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />
			QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />
			QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />
			QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />
			QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />QWEASD<br />
		</div>
	</div>
</div>

<template id="tpSelect">
	<div class="tpSelect">
		<input type="text" placeholder="Select option" />
		<i class="fa"></i>
		<div class="optionlist"></div>
	</div>
</template>

<script>
let template = {
	select: $('#tpSelect').html()
}

$('.menuprofile').click(function()
{
	$(this).toggleClass('change');
	$('#main-profile').toggleClass('active');
});

/*
$('.datatable').dataTable({
	dom: '<<"d-flex"fl><t><ip>>'
});
*/

$('select:not(.fg)').each(function()
{
	let that = $(this);
	$(this).addClass('fg hidden d-none');
	$(this).after(template.select);
	let optionlist = $(this).next().children('.optionlist');
	$(this).children().each(function()
	{
		if( that.hasClass('faicon') )
			optionlist.append('<span data-value="' + $(this).val() + '"><i class="fa ' + $(this).text() + '"></i> ' + $(this).text() + '</span>');
		else
			optionlist.append('<span data-value="' + $(this).val() + '">' + $(this).text() + '</span>');
	});
	
	$(this).next().children('input').val( $(this).find('option:selected').text() );
	if( $(this).is('[readonly]') )
		$(this).next().addClass('readonly');
});

$(document).on('change', 'select.fg', function()
{
	$(this).next().children('input').val( $(this).find('option:selected').text() );
});

$(document).on('keyup', '.tpSelect input', function()
{
	var regex = new RegExp($(this).val());
	$(this)
		.siblings('.optionlist')
		.children()
		.addClass('hidden d-none')
		.filter(function(){
			return regex.test($(this).text()); 
		})
		.removeClass('hidden d-none')
});

$(document).on('mousedown', '.tpSelect:not(.readonly) .optionlist span', function()
{
	$(this).parents('.tpSelect').children('input').val( $(this).text() );
	$(this).parents('.tpSelect').prev().val( $(this).data('value') );
});

$(document).on('focusout', '.tpSelect input', function()
{
	$(this).val( $(this).parent().prev().find('option:selected').text() );
});

$(document).on('focusin', '.tpSelect input', function()
{
	$(this)
		.siblings('.optionlist')
		.children()
		.removeClass('hidden d-none')
});
</script>

</body>
</html>


















